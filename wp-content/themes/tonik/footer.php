<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Recruitment_WP
 */
?>
</main>
<?php include('template-parts/footer/footer.php'); ?>
<?php wp_footer(); ?>
</body>
</html>

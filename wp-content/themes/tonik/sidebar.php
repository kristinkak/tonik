<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Recruitment_WP
 */
?>
<div class="sidebar__container">
	<p class="title">Share</p>
	<a class="share share__facebook" href="http://www.facebook.com/share.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>" target="_blank">
		<svg xmlns="http://www.w3.org/2000/svg" width="13.12" height="28" viewBox="0 0 6.56 14">
			<defs>
				<style>
					.cls-1 {
						fill: #fff;
						fill-rule: evenodd;
					}
				</style>
			</defs>
			<g id="facebook-24">
				<path id="XMLID_311_" class="cls-1" d="M1238.3,615.467h-6.07v-2.8h6.54Z" transform="translate(-1232.22 -608)"/>
				<path id="Facebook_3_" class="cls-1" d="M1233.63,610.8V622h2.8V611.267a0.433,0.433,0,0,1,.47-0.467h1.87V608h-2.16C1233.57,608,1233.63,610.45,1233.63,610.8Z" transform="translate(-1232.22 -608)"/>
			</g>
		</svg>

	</a>
	<a class="share share__linkedin" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>&source=<?php get_home_url() ;?>" target="_blank">
		<svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<title>linkedin</title>
			<desc>Created with Sketch.</desc>
			<defs></defs>
			<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<g id="Post" transform="translate(-1228.000000, -660.000000)" fill-rule="nonzero" fill="#FFFFFF">
					<g id="Group-10" transform="translate(1220.000000, 652.000000)">
						<g id="linkedin" transform="translate(8.000000, 8.000000)">
							<circle id="Oval" cx="1.75" cy="1.75" r="1.75"></circle>
							<path d="M3.20833333,4.66666667 L0.291666667,4.66666667 C0.130666667,4.66666667 0,4.79733333 0,4.95833333 L0,13.7083333 C0,13.8693333 0.130666667,14 0.291666667,14 L3.20833333,14 C3.36933333,14 3.5,13.8693333 3.5,13.7083333 L3.5,4.95833333 C3.5,4.79733333 3.36933333,4.66666667 3.20833333,4.66666667 Z" id="Shape"></path>
							<path d="M11.89825,4.26475 C10.6516667,3.83775 9.09241667,4.21283333 8.15733333,4.88541667 C8.12525,4.76 8.01091667,4.66666667 7.875,4.66666667 L4.95833333,4.66666667 C4.79733333,4.66666667 4.66666667,4.79733333 4.66666667,4.95833333 L4.66666667,13.7083333 C4.66666667,13.8693333 4.79733333,14 4.95833333,14 L7.875,14 C8.036,14 8.16666667,13.8693333 8.16666667,13.7083333 L8.16666667,7.42 C8.638,7.014 9.24525,6.8845 9.74225,7.09566667 C10.2240833,7.29925 10.5,7.79625 10.5,8.45833333 L10.5,13.7083333 C10.5,13.8693333 10.6306667,14 10.7916667,14 L13.7083333,14 C13.8693333,14 14,13.8693333 14,13.7083333 L14,7.87091667 C13.96675,5.474 12.8391667,4.58675 11.89825,4.26475 Z" id="Shape"></path>
						</g>
					</g>
				</g>
			</g>
		</svg>
	</a>
	<a class="share share__twitter" href="https://twitter.com/share?text=%20-<?php print(urlencode(the_title())); ?>" class="twitter" target="_blank">
		<svg width="16px" height="14px" viewBox="0 0 16 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<title>Twitter</title>
			<desc>Created with Sketch.</desc>
			<defs></defs>
			<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<g id="Post" transform="translate(-951.000000, -3955.000000)" fill="#FFFFFF">
					<g id="Group-8" transform="translate(0.000000, 3762.000000)">
						<g id="socials" transform="translate(915.000000, 192.000000)">
							<path d="M52,2.57842783 C51.4121031,2.84615385 50.7791278,3.02720597 50.1151611,3.1081016 C50.7932148,2.69206693 51.3134942,2.03238233 51.5586077,1.24653906 C50.9237542,1.63175635 50.2222222,1.91200193 49.4727945,2.06223667 C48.8755063,1.40833032 48.0218348,1 47.0770676,1 C45.265481,1 43.7957387,2.50716263 43.7957387,4.36583604 C43.7957387,4.62970988 43.8239127,4.88587938 43.8802606,5.13241844 C41.1520808,4.99181413 38.7338146,3.65318406 37.1138111,1.61442157 C36.8311322,2.11327796 36.6696015,2.69206693 36.6696015,3.30841459 C36.6696015,4.47562297 37.2490462,5.50607921 38.1299525,6.10990731 C37.5927687,6.09353557 37.0856371,5.9404117 36.6423666,5.69002046 L36.6423666,5.73143132 C36.6423666,7.36282653 37.7740212,8.7236066 39.2766332,9.03178043 C39.0014674,9.11074997 38.7112755,9.15023474 38.4116922,9.15023474 C38.2003874,9.15023474 37.9937782,9.13001083 37.793743,9.09052606 C38.211657,10.42723 39.4231379,11.4008667 40.8600106,11.4268689 C39.7368081,12.3302034 38.3205963,12.8685446 36.7832365,12.8685446 C36.5184011,12.8685446 36.2563832,12.8531359 36,12.8223185 C37.4528379,13.7757313 39.1789634,14.3333333 41.0318718,14.3333333 C47.0704936,14.3333333 50.3715443,9.20512821 50.3715443,4.75683159 C50.3715443,4.61044902 50.3687269,4.46406645 50.3630921,4.32057301 C51.0045196,3.8457927 51.5614251,3.25352113 52,2.57842783" id="Twitter"></path>
						</g>
					</g>
				</g>
			</g>
		</svg>
	</a>
</div>

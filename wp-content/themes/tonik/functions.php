<?php
/**
 * Recruitment WP functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Recruitment_WP
 */

if (!function_exists('recruitment_wp_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function recruitment_wp_setup()
	{
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		]);
	}
endif;
add_action('after_setup_theme', 'recruitment_wp_setup');

function register_my_menus()
{
	register_nav_menus(
		array(
			'header_menu' => 'Header Menu',
			'left_footer_menu' => 'Left Footer Menu',
			'middle_footer_menu' => 'Middle Footer Menu',
			'right_footer_menu' => 'Right Footer Menu'
		)
	);
}
add_action('init', 'register_my_menus');

function tonic_custom_logo()
{
	$defaults = array(
		'height' => 100,
		'width' => 400,
		'flex-height' => true,
		'flex-width' => true,
		'header-text' => array('site-title', 'site-description'),
	);
	add_theme_support('custom-logo', $defaults);
}

add_action('after_setup_theme', 'tonic_custom_logo');

function wps_mime_types($mimes)
{
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

add_filter('upload_mimes', 'wps_mime_types');

add_theme_support( 'post-thumbnails' );


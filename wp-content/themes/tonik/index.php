<?php get_header(); ?>
<div class="single__post single__post--sneakpeak">
	<?php
	$post_array = get_posts(array(
		'posts_per_page' => -1,
		'post_type' => 'post',
		'orderby' => 'date',
		'order' => 'ASC'

	));
	$i = 0;
	foreach ($post_array as $post) {
		$post_thumbnail_id = get_post_thumbnail_id($post->ID);
		$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id); ?>
		<div class="thumbnail"
			 style="background-image: url('<?php echo $post_thumbnail_url; ?>')"></div>
		<div class="info">
			<h2 class="title"><?php echo $post->post_title; ?></h2>
			<a class="link" href="<?php echo get_permalink($post->ID); ?>">czytaj
				<svg width="8px" height="14px" viewBox="0 0 8 14" version="1.1" xmlns="http://www.w3.org/2000/svg"
					 xmlns:xlink="http://www.w3.org/1999/xlink">
					<title>icon</title>
					<desc>Created with Sketch.</desc>
					<defs></defs>
					<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
					   stroke-linejoin="round">
						<g id="Post" transform="translate(-836.000000, -2896.000000)" stroke="#999999" stroke-width="2">
							<g id="In-Copy-CTA" transform="translate(316.000000, 2848.000000)">
								<g id="Group-3" transform="translate(183.000000, 29.000000)">
									<polyline id="icon"
											  transform="translate(341.000000, 26.000000) rotate(-180.000000) translate(-341.000000, -26.000000) "
											  points="344 32 338 26 344 20"></polyline>
								</g>
							</g>
						</g>
					</g>
				</svg></a>
		</div>
		<?php
	}
	?>
</div>
<?php get_footer(); ?>

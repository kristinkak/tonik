<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Recruitment_WP
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/resources/css/reset.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header class="main__container">
	<div class="header__logo">
		<?php $custom_logo_id = get_theme_mod('custom_logo');
		$image = wp_get_attachment_image_src($custom_logo_id, 'full');?>
		<a href="<?php echo home_url();?>">
			<img src="<?php echo $image[0]; ?>">
		</a>
	</div>
	<nav>
		<?php
		wp_nav_menu(array(
			'theme_location' => 'header_menu'
		)); ?>
	</nav>
</header>
<main class="main__container">


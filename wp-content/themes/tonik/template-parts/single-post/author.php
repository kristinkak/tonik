<div class="author main__container main__container--author">
	<div class="author__avatar">
		<?php echo get_avatar(get_the_author_meta('user_email')); ?>
	</div>
	<div class="author__info">
		<div class="author__info author__info--name">
			<?php
			$author = get_the_author();
			echo '<h4>'.$author.'</h4>';
			?>
		</div>
		<div class="author__info author__info--bio">
			<?php the_author_meta('description'); ?>
		</div>
	</div>
</div>
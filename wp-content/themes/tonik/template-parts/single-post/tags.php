<div class="tags__cloud main__container main__container--tags">
	<?php
	$tags = get_the_terms($post->ID, 'post_tag');
	if (!empty($tags)) {
		foreach ($tags as $tag) { ?>
			<span class="tag"><?php echo $tag->name; ?></span>
		<?php }
	} ?>
</div>
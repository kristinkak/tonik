<footer class="footer">
	<div class="main__container main__container--footer">
		<div class="container">
			<div class="footer__menu">
				<h4 class="footer__title"><?php _e('Product');?></h4>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'left_footer_menu'
				)); ?>
			</div>
			<div class="footer__menu">
				<h4 class="footer__title"><?php _e('Company');?></h4>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'middle_footer_menu'
				)); ?>
			</div>
			<div class="footer__menu">
				<h4 class="footer__title"><?php _e('Support');?></h4>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'right_footer_menu'
				)); ?>
			</div>
		</div>
	</div>
</footer>
<?php get_header(); ?>
<div class="single__post">
	<?php
	while (have_posts()) : the_post(); ?>
		<h1 class="single__post--header"><?php the_title(); ?></h1>
		<?php the_content();
	endwhile;
	?>
</div>
<?php
include ('template-parts/single-post/tags.php');
include ('template-parts/single-post/author.php');
get_sidebar();
?>
<?php get_footer(); ?>


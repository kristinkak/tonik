<?php
/**
Plugin Name: Tonik Posts
Plugin URI: https://kkdevelper.pl
Description: Edycja postów.
Version: 1.0
Author: Kristinka Klyap
Author URI: https://kkdevelper.pl
Text Domain: tonik-posts
*/

include_once( plugin_dir_path( __FILE__ ) . 'includes/menu.php');
include_once( plugin_dir_path( __FILE__ ) . 'includes/functions.php');
include_once( plugin_dir_path( __FILE__ ) . 'includes/shortcodes.php');
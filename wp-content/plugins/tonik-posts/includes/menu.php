<?php

function tonik_posts_metaboxes()
{
	add_meta_box('tonik_posts_ftitle', 'Pierwszy box: Tytuł', 'tonik_posts_ftitle', 'post', 'normal', 'default');
	add_meta_box('tonik_posts_fdescription', 'Pierwszy box: Opis', 'tonik_posts_fdescription', 'post', 'normal', 'default');
	add_meta_box('tonik_posts_flink', 'Pierwszy box: Link', 'tonik_posts_flink', 'post', 'normal', 'default');
	add_meta_box('tonik_posts_stitle', 'Drugi box: Tytuł', 'tonik_posts_stitle', 'post', 'normal', 'default');
	add_meta_box('tonik_posts_sdescription', 'Drugi box: Opis', 'tonik_posts_sdescription', 'post', 'normal', 'default');
	add_meta_box('wp_custom_attachment', 'Drugi box: Załącz plik', 'wp_custom_attachment', 'post', 'normal', 'default');

}

add_action('add_meta_boxes', 'tonik_posts_metaboxes');
function tonik_posts_ftitle()
{
	global $post;
	$html = '<p class="description">Użyj <b>[tonik_first_box]</b> żeby dodać pierwszy blok w dowolnym miejscu w edytorze';
	$html .= '</p>';
	echo $html;

	$value = get_post_meta($post->ID, '_ftitle', true);
	echo '<input type="text" name="_ftitle" value="' . $value . '" class="widefat" />';
}

function tonik_posts_fdescription()
{
	global $post;
	$value = get_post_meta($post->ID, '_fdescription', true);
	echo '<input type="text" name="_fdescription" value="' . $value . '" class="widefat" />';
}

function tonik_posts_flink()
{
	global $post;
	$value = get_post_meta($post->ID, '_flink', true);
	echo '<input type="text" name="_flink" value="' . $value . '" class="widefat" />';
}

function tonik_posts_stitle()
{
	global $post;
	$html = '<p class="description">Użyj <b>[tonik_second_box]</b> żeby dodać drugi blok w dowolnym miejscu w edytorze';
	$html .= '</p>';
	echo $html;
	$value = get_post_meta($post->ID, '_stitle', true);
	echo '<input type="text" name="_stitle" value="' . $value . '" class="widefat" />';
}

function tonik_posts_sdescription()
{
	global $post;
	$value = get_post_meta($post->ID, '_sdescription', true);
	echo '<input type="text" name="_sdescription" value="' . $value . '" class="widefat" />';
	wp_nonce_field(plugin_basename(__FILE__), 'postmeta_field');

}

function wp_custom_attachment()
{
	global $post;
	wp_nonce_field(plugin_basename(__FILE__), 'wp_custom_attachment_nonce');
	$value = get_post_meta($post->ID, 'wp_custom_attachment', true);
	if (!empty($value)) {
		echo '<p class="description">' . $value['url'] . '</p>';
	}
	$html = '<p class="description">';
	$html .= '</p>';
	$html .= '<input type="file" id="wp_custom_attachment" name="wp_custom_attachment" value="" size="25" />';

	echo $html;

}

function save_custom_meta_data($id)
{

	if (!wp_verify_nonce($_POST['wp_custom_attachment_nonce'], plugin_basename(__FILE__))) {
		return $id;
	}

	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return $id;
	}

	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $id)) {
			return $id;
		}
	} else {
		if (!current_user_can('edit_page', $id)) {
			return $id;
		}
	}

	if (!empty($_FILES['wp_custom_attachment']['name'])) {

		$supported_types = array('application/pdf');

		$arr_file_type = wp_check_filetype(basename($_FILES['wp_custom_attachment']['name']));
		$uploaded_type = $arr_file_type['type'];

		if (in_array($uploaded_type, $supported_types)) {

			$upload = wp_upload_bits($_FILES['wp_custom_attachment']['name'], null, file_get_contents($_FILES['wp_custom_attachment']['tmp_name']));

			if (isset($upload['error']) && $upload['error'] != 0) {
				wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
			} else {
				add_post_meta($id, 'wp_custom_attachment', $upload);
				update_post_meta($id, 'wp_custom_attachment', $upload);
			}
		} else {
			wp_die("The file type that you've uploaded is not a PDF.");
		}
	}

}

add_action('save_post', 'save_custom_meta_data');

function tonik_posts_save_meta($post_id, $post)
{
	if (isset($_POST['postmeta_field'])) {

		if (!wp_verify_nonce($_POST['postmeta_field'], plugin_basename(__FILE__))) {
			return $post->ID;
		}

		if (!current_user_can('edit_post', $post->ID)) return $post->ID;

		$posts_meta['_ftitle'] = $_POST['_ftitle'];
		$posts_meta['_fdescription'] = $_POST['_fdescription'];
		$posts_meta['_flink'] = $_POST['_flink'];
		$posts_meta['_stitle'] = $_POST['_stitle'];
		$posts_meta['_sdescription'] = $_POST['_sdescription'];
		$events_meta['_location'] = $_POST['_location'];


		foreach ($posts_meta as $key => $value) {
			if ($post->post_type == 'revision') return;
			if (get_post_meta($post->ID, $key, FALSE)) {
				update_post_meta($post->ID, $key, $value);
			} else {
				add_post_meta($post->ID, $key, $value);
			}

			if (!$value) delete_post_meta($post->ID, $key);
		}
	}
}

add_action('save_post', 'tonik_posts_save_meta', 1, 2);
function update_edit_form() {
	echo ' enctype="multipart/form-data"';
}
add_action('post_edit_form_tag', 'update_edit_form');
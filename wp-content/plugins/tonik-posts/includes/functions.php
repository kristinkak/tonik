<?php
function tonik_posts_first_box_content()
{
	$id = get_the_ID();
	$title = get_post_meta($id, "_ftitle", true);
	$subtitle = get_post_meta($id, "_fdescription", true);
	$link = get_post_meta($id, "_flink", true);

	?>
	<div class="editable_box">
		<div class="icon">
			<svg width="23px" height="22px" viewBox="0 0 23 22" version="1.1" xmlns="http://www.w3.org/2000/svg"
				 xmlns:xlink="http://www.w3.org/1999/xlink">
				<!-- Generator: Sketch 45.1 (43504) - http://www.bohemiancoding.com/sketch -->
				<title>slider-settings-24-px</title>
				<desc>Created with Sketch.</desc>
				<defs></defs>
				<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="Post" transform="translate(-491.000000, -1782.000000)" fill-rule="nonzero" fill="#4A90E2">
						<g id="Group" transform="translate(315.000000, 1719.000000)">
							<g id="Group-3" transform="translate(142.000000, 29.000000)">
								<g id="Group-2-Copy">
									<g id="slider-settings-24-px" transform="translate(34.000000, 34.000000)">
										<path
											d="M1,4 L4.5,4 L4.5,5.5 C4.5,5.77614237 4.72385763,6 5,6 L12,6 C12.2761424,6 12.5,5.77614237 12.5,5.5 L12.5,4 L22,4 C22.5522847,4 23,3.55228475 23,3 C23,2.44771525 22.5522847,2 22,2 L12.5,2 L12.5,0.5 C12.5,0.223857625 12.2761424,1.69088438e-17 12,0 L5,0 C4.72385763,-1.69088438e-17 4.5,0.223857625 4.5,0.5 L4.5,2 L1,2 C0.44771525,2 6.76353751e-17,2.44771525 0,3 C-6.76353751e-17,3.55228475 0.44771525,4 1,4 Z"
											id="Shape"></path>
										<path
											d="M22,10 L19,10 L19,8.5 C19,8.22385763 18.7761424,8 18.5,8 L11.5,8 C11.2238576,8 11,8.22385763 11,8.5 L11,10 L1,10 C0.44771525,10 6.76353751e-17,10.4477153 0,11 C-6.76353751e-17,11.5522847 0.44771525,12 1,12 L11,12 L11,13.5 C11,13.7761424 11.2238576,14 11.5,14 L18.5,14 C18.7761424,14 19,13.7761424 19,13.5 L19,12 L22,12 C22.5522847,12 23,11.5522847 23,11 C23,10.4477153 22.5522847,10 22,10 Z"
											id="Shape"></path>
										<path
											d="M22,18 L12.5,18 L12.5,16.5 C12.5,16.2238576 12.2761424,16 12,16 L5,16 C4.72385763,16 4.5,16.2238576 4.5,16.5 L4.5,18 L1,18 C0.44771525,18 6.76353751e-17,18.4477153 0,19 C-6.76353751e-17,19.5522847 0.44771525,20 1,20 L4.5,20 L4.5,21.5 C4.5,21.7761424 4.72385763,22 5,22 L12,22 C12.2761424,22 12.5,21.7761424 12.5,21.5 L12.5,20 L22,20 C22.5522847,20 23,19.5522847 23,19 C23,18.4477153 22.5522847,18 22,18 Z"
											id="Shape"></path>
									</g>
								</g>
							</g>
						</g>
					</g>
				</g>
			</svg>
		</div>
		<div class="editable_box--container">
			<h4><a href="<?php echo $link; ?>"><?php echo $title; ?>
					<svg width="8px" height="14px" viewBox="0 0 8 14" version="1.1" xmlns="http://www.w3.org/2000/svg"
						 xmlns:xlink="http://www.w3.org/1999/xlink">
						<title>icon</title>
						<desc>Created with Sketch.</desc>
						<defs></defs>
						<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
						   stroke-linecap="round" stroke-linejoin="round">
							<g id="Post" transform="translate(-836.000000, -2896.000000)" stroke="#999999"
							   stroke-width="2">
								<g id="In-Copy-CTA" transform="translate(316.000000, 2848.000000)">
									<g id="Group-3" transform="translate(183.000000, 29.000000)">
										<polyline id="icon"
												  transform="translate(341.000000, 26.000000) rotate(-180.000000) translate(-341.000000, -26.000000) "
												  points="344 32 338 26 344 20"></polyline>
									</g>
								</g>
							</g>
						</g>
					</svg>
				</a>
			</h4>
			<h6><?php echo $subtitle; ?></h6>
		</div>
	</div>
	<?php
}

function tonik_posts_second_box_content()
{
	$id = get_the_ID();
	$title = get_post_meta($id, "_stitle", true);
	$subtitle = get_post_meta($id, "_sdescription", true);
	$file = get_post_meta($id, 'wp_custom_attachment', true);

	?>
	<div class="editable_box">
		<div class="icon">
			<svg width="20px" height="21px" viewBox="0 0 20 21" version="1.1" xmlns="http://www.w3.org/2000/svg"
				 xmlns:xlink="http://www.w3.org/1999/xlink">
				<!-- Generator: Sketch 45.1 (43504) - http://www.bohemiancoding.com/sketch -->
				<title>download-24-px</title>
				<desc>Created with Sketch.</desc>
				<defs></defs>
				<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<g id="Post" transform="translate(-534.000000, -2912.000000)" fill-rule="nonzero" fill="#4A90E2">
						<g id="In-Copy-CTA" transform="translate(499.000000, 2877.000000)">
							<g id="Group-3">
								<g id="Group-2-Copy">
									<g id="download-24-px" transform="translate(35.000000, 35.000000)">
										<path
											d="M19.5,21 L1.5,21 C0.95,21 0,20.55 0,20 L0,15 C-6.76353751e-17,14.4477153 0.44771525,14 1,14 C1.55228475,14 2,14.4477153 2,15 L2,19 L18,19 L18,15 C18,14.4477153 18.4477153,14 19,14 C19.5522847,14 20,14.4477153 20,15 L20,20 C20,20.55 20.05,21 19.5,21 Z"
											id="Shape"></path>
										<path
											d="M18,7.55 C17.6099625,7.16227641 16.9800375,7.16227641 16.59,7.55 L11.48,12.47 L11.48,0.89 C11.4759381,0.63641703 11.3650731,0.396317585 11.1746805,0.228772112 C10.984288,0.0612266387 10.7320415,-0.0182120292 10.48,0.01 L10.48,0.01 C10.2296539,-0.0180832283 9.97899706,0.0600422093 9.78894052,0.225391402 C9.59888398,0.390740594 9.48682698,0.628176178 9.48,0.88 L9.48,12.4 L4.48,7.54 C4.21919612,7.2845551 3.84192516,7.18765149 3.49030145,7.28579182 C3.13867774,7.38393216 2.86612127,7.66220659 2.77530144,8.01579183 C2.68448161,8.36937707 2.78919611,8.7445551 3.05,9 L9.74,15.51 L9.74,15.51 L9.74,15.51 C9.80175771,15.5576643 9.86888082,15.5979382 9.94,15.63 L9.94,15.63 L10.08,15.63 L10.13,15.63 L10.32,15.63 L10.51,15.63 L10.51,15.63 C10.6970437,15.5958931 10.8704883,15.5091709 11.01,15.38 L17.94,9 C18.1467648,8.81956395 18.2702923,8.56208532 18.2816381,8.28789537 C18.2929839,8.01370542 18.1911508,7.74690153 18,7.55 Z"
											id="Shape"></path>
									</g>
								</g>
							</g>
						</g>
					</g>
				</g>
			</svg>
		</div>
		<div class="editable_box--container">
			<h4>
				<a href="<?php echo $file['url']; ?>" target="_blank" download><?php echo $title; ?>
					<svg width="8px" height="14px" viewBox="0 0 8 14" version="1.1" xmlns="http://www.w3.org/2000/svg"
						 xmlns:xlink="http://www.w3.org/1999/xlink">
						<title>icon</title>
						<desc>Created with Sketch.</desc>
						<defs></defs>
						<g id="Web" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
						   stroke-linejoin="round">
							<g id="Post" transform="translate(-836.000000, -2896.000000)" stroke="#999999" stroke-width="2">
								<g id="In-Copy-CTA" transform="translate(316.000000, 2848.000000)">
									<g id="Group-3" transform="translate(183.000000, 29.000000)">
										<polyline id="icon"
												  transform="translate(341.000000, 26.000000) rotate(-180.000000) translate(-341.000000, -26.000000) "
												  points="344 32 338 26 344 20"></polyline>
									</g>
								</g>
							</g>
						</g>
					</svg>
				</a>
			</h4>
			<h6><?php echo $subtitle; ?></h6>
		</div>
	</div>
	<?php
}
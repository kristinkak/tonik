<?php

function tonik_first_box_shortcode()
{
	ob_start();
	tonik_posts_first_box_content();
	$output = ob_get_clean();
	return $output;
}

add_shortcode('tonik_first_box', 'tonik_first_box_shortcode');

function tonik_second_box_shortcode()
{
	ob_start();
	tonik_posts_second_box_content();
	$output = ob_get_clean();
	return $output;
}

add_shortcode('tonik_second_box', 'tonik_second_box_shortcode');